from marshmallow import Schema, fields


class CampaignSchema(Schema):
    """schema definition for response with date window and campaign param"""
    total_clicks = fields.Integer(dump_only=True)
    campaign = fields.Str(dump_only=True)
    start_date = fields.Str(dump_only=True)
    end_date = fields.Str(dump_only=True)


class CampaignSchema1(Schema):
    """schema definition for response with campaign param only"""
    total_clicks = fields.Integer(dump_only=True)
    campaign = fields.Str(dump_only=True)
