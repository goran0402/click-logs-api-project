from flask import Flask
from flask_smorest import  Api
from db_resources import db
from api_resources.campaign import blp as CampaignBlueprint

def create_app(db_url=None):

    app = Flask(__name__)

    app.config["PROPAGATE_EXCEPTIONS"] = True
    app.config["API_TITLE"] = "Click_logs REST API"
    app.config["API_VERSION"] = "v1"
    app.config["OPENAPI_VERSION"] = "3.0.3"
    app.config["OPENAPI_URL_PREFIX"] = "/"
    app.config["OPENAPI_SWAGGER_UI_PATH"] = "/swagger-ui"
    app.config["OPENAPI_SWAGGER_UI_URL"] = "https://cdn.jsdelivr.net/npm/swagger-ui-dist/"
    api = Api(app)

    @app.before_first_request
    def create_tables():
        """create database, table and data import"""
        db.create_table()


    api.register_blueprint(CampaignBlueprint)

    return  app
