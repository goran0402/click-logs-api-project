from flask.views import MethodView
from flask_smorest import Blueprint, abort
from schema import  CampaignSchema, CampaignSchema1
from db_resources import db
import datetime

blp = Blueprint( "campaign", __name__, description="Amount of clicks" )


@blp.route( "/camapaign_and_date/<campaign>,<start_date>,<end_date>" )
class Campaign( MethodView ):
    @blp.response( 200, CampaignSchema )
    def get(self, campaign,start_date="",end_date=""):
        try:  #check if date format is ok
            start_date_chk = datetime.datetime.strptime(start_date ,"%Y-%m-%d %H:%M:%S")
            end_date_chk = datetime.datetime.strptime( end_date, "%Y-%m-%d %H:%M:%S" )
            total_click = db.get_campaign_time_click(campaign,  start_date_chk,end_date_chk)
            print( total_click,start_date,end_date )
            return {"total_clicks": total_click, "campaign": campaign,"start_date":start_date,"end_date":end_date}
        except ValueError :
            abort( 400, message="Bad request, date format !")



@blp.route( "/camapaign/<campaign>" )
class Campaign( MethodView ):
    @blp.response( 200, CampaignSchema1 )
    def get(self, campaign):
        total_click = db.get_campaing_click( campaign )
        print(total_click)
        return { "total_clicks": total_click,"campaign":campaign }