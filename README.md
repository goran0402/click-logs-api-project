****Click-logs API project****
.
**Main libraries used:**

Flask
Flask-smorest
Pandas

**Project structure:**

.
├── README.md
├── app.py
├── api_resources
│   ├── __init__.py
│   └── campaign.py
├── db_resources
│   ├── __init__.py
│   ├── config_db.py
│   └── db.py
├── click_log.csv
├── Dockerfile
├── schema.py
└── requirement.txt

api_resources/campaign.py : api endpoint

db_resources/db.py : database endpoint

db_resources/config_db.py: dictionary: database name, table_name, csv path

Dockerfile - docker image build

schema.py : api response schema

requirement.txt: libraries


app.py - flask application initialization.



Dockerfile - docker image build

**Running - localy**

Clone repository.

run python virtual env: python 3.6.9 -m venv .venv_flask 

start virtual env: . venv_flask/bin/activate

pip install requirement.txt

Run following command:

flask run

**Docker image run**

docker build: docker build -t click-logs-api-flask-python .

docker run: docker run -p 5005:5000 -w /app -v "$(pwd):/app"   click-logs-api-flask-python


**campaign endpoint**

swagger:
http://127.0.0.1:5005/swagger-ui



GET http://127.0.0.1:5000/campaign/4510461
```json
{
  "campaign": "string",
  "total_clicks": 0
}
```

GET http://127.0.0.1:5005/camapaign_and_date/4510461,2021-11-07%2003%3A10%3A00,2021-11-07%2003%3A30%3A00
```json
{
  "start_date": "string",
  "end_date": "string",
  "campaign": "string",
  "total_clicks": 0
}
```


