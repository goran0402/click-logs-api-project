import sqlite3
import pandas as pd
import db_resources.config_db as config

"""
@param db_file: database name
@param table_name: table_name for click_logs data
@param csv_path: csv file path
"""

db_file = config.conf["db_file"]
table_name = config.conf["table_name"]
csv_path = config.conf["csv_path"]


def create_connection(db_file):
    #Return database connection
    conn = None

    try:
        conn = sqlite3.connect( db_file )
    except sqlite3.Error as e:
        print( e )
    return conn


def create_table():
    db_file = config.conf["db_file"]
    table_name = config.conf["table_name"]
    conn = create_connection( db_file )
    c = conn.cursor()
    c.execute( f'''
          CREATE TABLE IF NOT EXISTS {table_name}
          ([id] INTEGER PRIMARY KEY, 
          [timestamp] TEXT,
          [type] TEXT,
          [campaign] INTEGER,
          [banner] INTEGER,
          [content_type] INTEGER,
          [network] INTEGER,
          [browser] INTEGER,
          [operating_system] INTEGER,
          [country] INTEGER,
          [state] INTEGER,
          [city] INTEGER 
           )
          ''' )
    conn.commit()
    conn.close()
    import_table_data()


def import_table_data():
    #import data from csv into table
    df = pd.read_csv( csv_path, delimiter=",", skip_blank_lines=True, header=0 )
    conn = create_connection( db_file )
    df.to_sql( table_name, conn, if_exists='replace', index=False )
    conn.commit()
    conn.close()
    return  print("import done")


def get_campaing_click(campaing_id):
    #Return total clicks for specific campaign
    conn = create_connection( db_file )
    c = conn.cursor()
    c.execute( f'''
              SELECT
              count(*) as num_of_click
              From click_logs
              where campaign = {campaing_id}
             
              ''' )

    num_of_click = c.fetchone()[0]
    conn.commit()
    conn.close()
    print( num_of_click )
    return num_of_click


def get_campaign_time_click(campaing_id, start_date, end_date):
    #Return total clicks for specific campaign and date window
    conn = create_connection( db_file )
    c = conn.cursor()
    c.execute( f'''
              SELECT
              count(*) as num_of_click
              From {table_name}
              where campaign={campaing_id}
              and  DATETIME(timestamp,'unixepoch') between '{start_date}' and '{end_date}'
              ''' )

    num_of_click = c.fetchone()[0]
    print( num_of_click )
    conn.commit()
    conn.close()
    return num_of_click


def main():
    #create_table()
    #import_table_data()
    #num_of_click = get_campaing_click( 4510461 )
    #get_campaign_time_click( db_file, table_name, campaing_id=4510461, start_date='2021-11-07 03:10:00',
    #                         end_date='2021-11-07 03:30:00' )
    # print( num_of_click )
    pass

if __name__ == '__main__':
    main()
